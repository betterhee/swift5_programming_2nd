import Swift

// 코드 26-8 where 절을 활용한 제네릭 타입의 메서드 제약 추가

// 코드22-8의 Stack 제네릭 구조체
struct Stack<Element> {
    var items = [Element]()
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}

// 익스텐션을 사용한 제네릭 타입 확장
extension Stack {
    func sorted() -> [Element] where Element: Comparable {
        return items.sorted()
    }
}
