import Swift

// 코드 10-17 클로저를 대체할 수 있는 키 경로 표현
struct Person {
    let name: String
    let nickname: String?
    let age: Int
    
    var isAdult: Bool {
        return age > 18
    }
}

let yagom: Person = Person(name: "yagom", nickname: "bear", age: 100)
let hana: Person = Person(name: "hana", nickname: "na", age: 100)
let happy: Person = Person(name: "happy", nickname: nil, age: 3)

let family: [Person] = [yagom, hana, happy]
let names: [String] = family.map(\.name)    // ["yagom", "hana", "happy"]
let nicknames: [String] = family.compactMap(\.nickname) // ["bear", "na"]
let adults: [String] = family.filter(\.isAdult).map(\.name) // ["yagom", "hana"]
